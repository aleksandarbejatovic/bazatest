from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine, Column, Integer, String, Sequence
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import exists

Base = declarative_base()

engine = create_engine('sqlite:///localhost:5432', echo=True)


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, Sequence('user_id_seq'), primary_key=True)
    name = Column(String)
    fullname = Column(String)
    nickname = Column(String)
   # counter = Column(default=0)  # field counter koji ima numericku vrije. i pocinje od nule, kako da bude isti za sve
    #i kako da ga uvecavam ??

    def __repr__(self):
        return "<User(name='%s', fullname='%s', nickname='%s')>" % (
                            self.name, self.fullname, self.nickname)


counter = 0

Base.metadata.create_all(engine)  # kreiranje tabele

Session = sessionmaker(bind=engine)  # vraca klasu vezuci za nju "mehanizam" proslijedjen kao argument
session = Session()  # kreiranje instance klase Session


# ako postoje za svakog usera inkrementovati counter +1 (refresh i commit)
ed_user = User(name='ed', fullname='Ed Jones', nickname='edsnickname')
ed2_user = User(name='ed', fullname='Goran Ng', nickname='goranica')
ed3_user = User(name='de', fullname='Nbg Dansa', nickname='danzakard')


def input(session, counter, new_user):  # korisnici ne postoje napravi ih, ako postoje, samo ispisi, kakko optimizovati?
    testcount = 0
    for row in session.query(User).all():
        print(row.id)
        if new_user.name == row.name and new_user.nickname == row.nickname and new_user.fullname == row.fullname:
            testcount -= 1  # umanjivanje za jedan, kako sigurno ne bi unijeli postojeceg usera
        else:
            testcount += 1
    print("TESTCOUNT: ", testcount)
    if testcount == counter:
        session.add(new_user)
        counter += 1
        print("Novi user!")
    else:
        print(new_user)

    return session, counter


session, counter = input(session, counter, ed3_user)
session, counter = input(session, counter, ed_user)
session, counter = input(session, counter, ed2_user)


print("COUNTER:", counter)
# session.commit()

print(session.query(User).all())

# napraviti migraciju koja ce dodati novo polje neki datum (Alembic)